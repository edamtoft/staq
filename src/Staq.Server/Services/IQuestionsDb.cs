﻿using System;
using System.Threading.Tasks;
using Staq.Server.Models;
using System.Collections.Generic;

namespace Staq.Server.Services
{
  public interface IQuestionsDb
  {
    Task<string> Post(Question question);

    Task<List<Question>> GetUnansweredQuestions();

    Task<string> PostAnswer(string questionId, Answer answer);

    Task<(Question, List<Answer>)> Get(string questionId);

    Task<List<Question>> Search(string query);

    Task<Message> SearchExactText(string query);
  }
}