﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Staq.Server.Services
{
  public class HipchatNotificationService : IDisposable, INotificationService
  {
    private readonly HttpClient _client;
    private readonly string _room;
    private readonly string _token;

    public HipchatNotificationService(string room, string token)
    {
      _room = room;
      _token = token;
      _client = new HttpClient();
    }

    public async Task SendNotification(string message)
    {
      var model = new
      {
        color = "green",
        message,
        notify = false,
        message_format = "text",
      };
      var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.Default, "application/json");

      await _client.PostAsync($"https://dealeron.hipchat.com/v2/room/{_room}/notification?auth_token={_token}", content);
    }

    public void Dispose()
    {
      _client.Dispose();
    }
  }
}
