﻿using Microsoft.Azure.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Staq.Server.Services
{
  public class IndexCollection
  {
    public ISearchIndexClient Questions { get; set; }
    public ISearchIndexClient Answers { get; set; }
  }
}
