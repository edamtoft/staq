﻿using System.Threading.Tasks;

namespace Staq.Server.Services
{
  public interface INotificationService
  {
    Task SendNotification(string message);
  }
}