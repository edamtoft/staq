﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Staq.Server.Models;
using Microsoft.Azure.Search;
using Microsoft.Azure.Search.Models;

namespace Staq.Server.Services
{
  public class QuestionsDb : IQuestionsDb
  {
    private readonly IndexCollection _indexes;

    public QuestionsDb(IndexCollection indexes)
    {
      _indexes = indexes;
    }

    public async Task<string> Post(Question question)
    {
      var conflict = await SearchExactText(question.Body);

      if (conflict != null)
      {
        throw new DuplicateMessageException("Found exact text duplicate", conflict);
      }

      var questionId = Guid.NewGuid().ToString();
      question.Id = questionId;
      question.When = DateTimeOffset.Now;
      question.Tags = new List<string>();
      await _indexes.Questions.Documents.IndexAsync(new IndexBatch<Question>(new[]
      {
        IndexAction.MergeOrUpload(question)
      }));
      return questionId;
    }

    public async Task<List<Question>> GetUnansweredQuestions()
    {
      var searchParameters = new SearchParameters
      { 
        Filter = "not Answers/any()",
        OrderBy = new List<string> { "When desc" },
        Top = 10
      };
      var questions = await _indexes.Questions.Documents.SearchAsync<Question>("", searchParameters);
      return questions.Results.Select(x => x.Document).ToList();
    }

    public async Task<List<Question>> Search(string query)
    {
      var questions = await _indexes.Questions.Documents.SearchAsync<Question>(query);
      return questions.Results.Select(x => x.Document).ToList();
    }

    public async Task<Message> SearchExactText(string query)
    {
      var escapedQuery = query.Replace("'", "''");
      var questionResult = 
        (await _indexes.Questions.Documents.SearchAsync<Question>("", new SearchParameters() { Filter = $"Body eq '{escapedQuery}'"}))
        .Results.FirstOrDefault()?.Document;

      if (questionResult != null) return questionResult;

      return
        (await _indexes.Answers.Documents.SearchAsync<Answer>("", new SearchParameters() { Filter = $"Body eq '{escapedQuery}'" }))
        .Results.FirstOrDefault()?.Document;
    }

    public async Task<(Question,List<Answer>)> Get(string questionId)
    {
      var question = _indexes.Questions.Documents.GetAsync<Question>(questionId);
      var searchParameters = new SearchParameters
      {
        Filter = $"QuestionId eq '{questionId}'",
        OrderBy = new List<string> { "When desc" },
        Top = 10
      };
      var answers = await _indexes.Answers.Documents.SearchAsync<Answer>("", searchParameters);
      return (await question, answers.Results.Select(x => x.Document).ToList());
    }

    public async Task<string> PostAnswer(string questionId, Answer answer)
    {
      var question = await _indexes.Questions.Documents.GetAsync<Question>(questionId);

      if (question == null)
      {
        throw new KeyNotFoundException($"Question {questionId} not found");
      }

      var conflict = await SearchExactText(answer.Body);

      if(conflict != null)
      {
        throw new DuplicateMessageException("Found exact text duplicate", conflict);
      }

      var answerId = Guid.NewGuid().ToString();

      answer.QuestionId = question.Id;
      answer.Id = answerId;
      answer.When = DateTimeOffset.Now;
      question.Answers.Add(answer.Id);


      await _indexes.Answers.Documents.IndexAsync(new IndexBatch<Answer>(new[] { IndexAction.Upload(answer) }));
      await _indexes.Questions.Documents.IndexAsync(new IndexBatch<Question>(new[] { IndexAction.Merge(question) }));

      return answerId;
    }

    public class DuplicateMessageException : Exception
    {
      public readonly Message ConflictingMessage;

      public DuplicateMessageException(string error, Message conflictingMessage = null) : base(error)
      {
        ConflictingMessage = conflictingMessage;
      }
    }
  }
}
