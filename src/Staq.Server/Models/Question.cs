﻿using Microsoft.Azure.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Staq.Server.Models
{
  public class Question : Message
  {
    [IsFacetable,IsFilterable]
    public List<string> Tags { get; set; }

    [IsFilterable]
    public List<string> Answers { get; set; }
  }
}
