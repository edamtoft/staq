﻿using Microsoft.Azure.Search;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Staq.Server.Models
{
  public abstract class Message
  {
    [Key]
    public string Id { get; set; }

    [IsFilterable,IsSearchable]
    public string Body { get; set; }

    [IsFilterable,IsSortable]
    public DateTimeOffset When { get; set; }

    [IsFilterable,IsFacetable]
    public string Who { get; set; }

    public List<string> Attachments { get; set; }
  }
}
