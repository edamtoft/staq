﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Staq.Server.Models.Firebase
{
  public class Installation
  {
    public string OauthId { get; set; }
    public string OauthSecret { get; set; }
  }
}
