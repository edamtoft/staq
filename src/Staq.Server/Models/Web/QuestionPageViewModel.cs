﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Staq.Server.Models.Web
{
  public class QuestionPageViewModel : StaqViewModel
  {
    public Question Question { get; set; }
    public List<Answer> Answers { get; set; }
  }
}
