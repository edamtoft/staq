﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Staq.Server.Models.Web
{
  public abstract class StaqViewModel
  {
    public string SearchQuery { get; set; }
  }
}
