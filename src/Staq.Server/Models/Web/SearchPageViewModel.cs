﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Staq.Server.Models.Web
{
  public class SearchPageViewModel : StaqViewModel
  {
    public List<Question> Questions { get; set; }
  }
}
