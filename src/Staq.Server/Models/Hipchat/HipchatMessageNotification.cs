﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Staq.Server.Models.Hipchat
{
  public class HipchatMessageNotification
  {
    public string Event { get; set; }

    public HipchatMessageItem Item { get; set; }
  }
}
