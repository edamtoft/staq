﻿namespace Staq.Server.Models.Hipchat
{
  public class HipchatMessageItem
  {
    public HipchatMessageData Message { get; set; }
  }
}