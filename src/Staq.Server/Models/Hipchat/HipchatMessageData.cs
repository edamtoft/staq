﻿namespace Staq.Server.Models.Hipchat
{
  public class HipchatMessageData
  {
    public HipchatUser From { get; set; }
    public string Message { get; set; }
  }
}