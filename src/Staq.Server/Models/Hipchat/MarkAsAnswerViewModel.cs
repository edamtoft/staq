﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Staq.Server.Models.Hipchat
{
  public class MarkAsAnswerViewModel
  {
    public List<Question> Questions { get; set; }
  }
}
