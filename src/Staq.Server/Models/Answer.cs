﻿using Microsoft.Azure.Search;

namespace Staq.Server.Models
{
  public class Answer : Message
  {
    [IsSearchable, IsFilterable]
    public string QuestionId { get; set; }
  }
}