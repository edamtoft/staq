﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Staq.Server.Models;
using Staq.Server.Services;
using System.Net;

namespace Staq.Server.Controllers
{
  public class ApiController : Controller
  {
    private readonly IQuestionsDb _db;
    private readonly INotificationService _notifications;

    public ApiController(IQuestionsDb db, INotificationService notifications)
    {
      _db = db;
      _notifications = notifications;
    }

    [HttpPost]
    public async Task<IActionResult> Ask([FromBody]Question question)
    {
      try
      {
        var questionId = await _db.Post(question);
      await _notifications.SendNotification($"{question.Who} posted a question. Once this question is answered, select \"Mark as Answer\" in the actions dropdown for the answer. To view this question, see {Request.Scheme}://{Request.Host}/question/{questionId}.");
        return Json(new { questionId });
      }
      catch (QuestionsDb.DuplicateMessageException ex)
      {
        Response.StatusCode = (int)HttpStatusCode.Conflict;
        return Json(ex.ConflictingMessage);
      }
    }

    [HttpGet]
    public async Task<IActionResult> Unanswered()
    {
      return Json(await _db.GetUnansweredQuestions());
    }

    [HttpGet("[controller]/[action]/{questionText}")]
    public async Task<IActionResult> SearchFullText(string questionText)
    {
      var result = await _db.SearchExactText(questionText);
      if (result == null) return NotFound($"No messages matching \"{questionText}\" found");
      return Json(result);
    }

    [HttpPost("[controller]/[action]/{questionId}")]
    public async Task<IActionResult> Answer(string questionId, [FromBody]Answer answer)
    {
      try
      {
        var answerId = await _db.PostAnswer(questionId, answer);
        await _notifications.SendNotification($"{answer.Who} provided an answer. To see all answers see {Request.Scheme}://{Request.Host}/question/{questionId}.");
        return Json(new { answerId });
      }
      catch (KeyNotFoundException)
      {
        return NotFound();
      }
      catch (QuestionsDb.DuplicateMessageException ex)
      {
        Response.StatusCode = (int)HttpStatusCode.Conflict;
        return Json(ex.ConflictingMessage);
      }
    }
  }
}
