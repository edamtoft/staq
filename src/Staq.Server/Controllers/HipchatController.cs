﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Staq.Server.Services;
using Staq.Server.Models.Hipchat;
using Staq.Server.Models;

namespace Staq.Server.Controllers

{

  public class HipchatController : Controller
  {
    private readonly IQuestionsDb _db;
    private readonly INotificationService _notifications;

    public HipchatController(IQuestionsDb db, INotificationService notifications)
    {
      _db = db;
      _notifications = notifications;
    }


    [HttpGet]
    public IActionResult Index() => View();

    [HttpGet]
    public IActionResult Capabilities()
    {
      return Json(new
      {
        Key = "STAQ",
        Name = "STAQ Question and Answers Platform",
        Description = "A low friction way to capture and retain domain knowledge using the tools you already use.",
        Links = new
        {
          HomePage = $"{Request.Scheme}://{Request.Host}/hipchat",
          Self = $"{Request.Scheme}://{Request.Host}/hipchat/{nameof(Capabilities)}",
        },
        Capabilities = new
        {
          Action = new[]
          {
            new
            {
              Key = "MarkAsQuestionAction",
              Location = "hipchat.message.action",
              Name = new { Value = "Mark as Question" },
              Target = new { Key = "MarkAsQuestionDialog" }
            },
            new
            {
              Key = "MarkAsAnswerAction",
              Location = "hipchat.message.action",
              Name = new { Value = "Mark as Answer" },
              Target = new { Key = "MarkAsAnswerDialog" }
            }
          },
          Dialog = new[]
          {
            new
            {
              Key = "MarkAsQuestionDialog",
              Authentication = "none",
              Title = new { Value = "Mark as Question" },
              Url = $"{Request.Scheme}://{Request.Host}/hipchat/{nameof(MarkAsQuestion)}"
            },
            new
            {
              Key = "MarkAsAnswerDialog",
              Authentication = "none",
              Title = new { Value = "Mark as Answer" },
              Url = $"{Request.Scheme}://{Request.Host}/hipchat/{nameof(MarkAsAnswer)}"
            }
          },
          Webhook = new []
          {
            new
            {
              Url = $"{Request.Scheme}://{Request.Host}/hipchat/{nameof(OnQuestion)}",
              Authentication = "none",
              Pattern = "^/question",
              Event = "room_message",
              Name = "Question"
            },
            new
            {
              Url = $"{Request.Scheme}://{Request.Host}/hipchat/{nameof(OnAnswer)}",
              Authentication = "none",
              Pattern = "^/answer",
              Event = "room_message",
              Name = "Answer"
            }
          },
          HipchatApiConsumer = new
          {
            FromName = "Team 13",
            Scopes = new[] { "send_notification" }
          },
          Installable = new
          {
            AllowGlobal = false,
            AllowRoom = true,
          }
        }
      });
    }

    [HttpPost]
    public async Task<IActionResult> OnQuestion([FromBody]HipchatMessageNotification notification)
    {
      var question = new Question
      {
        Who = notification?.Item?.Message?.From?.Name,
        Body = notification?.Item?.Message?.Message.Substring("/question".Length).Trim()
      };
      var questionId = await _db.Post(question);
      await _notifications.SendNotification($"{question.Who} posted a question. Once this question is answered, select \"Mark as Answer\" in the actions dropdown for the answer. To view this question, see {Request.Scheme}://{Request.Host}/question/{questionId}");
      return Ok();
    }

    [HttpPost]
    public async Task<IActionResult> OnAnswer([FromBody]HipchatMessageNotification notification)
    {
      var openQuestions = await _db.GetUnansweredQuestions();
      if (!openQuestions.Any())
      {
        return StatusCode(422);
      }
      var answer = new Answer
      {
        Who = notification?.Item?.Message?.From?.Name,
        Body = notification?.Item?.Message?.Message.Substring("/answer".Length).Trim()
      };
      var questionId = openQuestions.First().Id;
      await _notifications.SendNotification($"{answer.Who} provided an answer. To see all answers see {Request.Scheme}://{Request.Host}/question/{questionId}");

      await _db.PostAnswer(questionId, answer);
      return Ok();
    }

    [HttpGet]
    public IActionResult MarkAsQuestion() => View();

    [HttpGet]
    public async Task<IActionResult> MarkAsAnswer()
    {
      return View(new MarkAsAnswerViewModel
      {
        Questions = await _db.GetUnansweredQuestions()
      });
    }
  }
}


