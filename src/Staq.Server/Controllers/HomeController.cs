﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Staq.Server.Services;
using Staq.Server.Models.Web;

namespace Staq.Server.Controllers
{
  public class HomeController : Controller
  {
    private readonly IQuestionsDb _db;

    public HomeController(IQuestionsDb db)
    {
      _db = db;
    }

    [HttpGet]
    public async Task<IActionResult> Index(string query)
    {
      var results = await _db.Search(query);

      return View(new SearchPageViewModel { SearchQuery = query, Questions = results });
    }

    [HttpGet("[action]/{questionId}")]
    public async Task<IActionResult> Question(string questionId, string query)
    {
      var (question, answers) = await _db.Get(questionId);

      if (question == null)
      {
        return NotFound();
      }

      return View(new QuestionPageViewModel { SearchQuery = query, Question = question, Answers = answers });
    }
  }
}
