﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Staq.Server.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Azure.Search;
using Microsoft.Azure.Search.Models;
using Staq.Server.Models;
using System.ComponentModel;

namespace Staq.Server
{
  public class Startup
  {
    private readonly IConfigurationRoot _config;

    public Startup(IHostingEnvironment env)
    {
      _config = new ConfigurationBuilder()
        .SetBasePath(env.ContentRootPath)
        .AddJsonFile("appsettings.json", optional: false)
        .AddEnvironmentVariables()
        .Build();
    }

    public void ConfigureServices(IServiceCollection services)
    {
      var credentials = new SearchCredentials(_config["azureSearchKey"]);

      var azureSearchClient = new SearchServiceClient("Staq", credentials);

      //azureSearchClient.Indexes.CreateOrUpdate(new Index("questions", FieldBuilder.BuildForType<Question>()));
      //azureSearchClient.Indexes.CreateOrUpdate(new Index("answers", FieldBuilder.BuildForType<Answer>()));

      services.AddMvc();

      services.AddSingleton<INotificationService>(_ => new HipchatNotificationService("4110234", "kSvYcyRrHyVSmbd7S9Ig8XE1J1D6a6adOHkNqUDV"));

      services.AddScoped(_ =>
      {
      
        return new IndexCollection
        {
          Questions = new SearchIndexClient("Staq", "questions", credentials),
          Answers = new SearchIndexClient("Staq", "answers", credentials)
        };
      });

      services.AddScoped<IQuestionsDb, QuestionsDb>();
    }

    public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
    {
      loggerFactory.AddConsole();

      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }
      app.UseStaticFiles();
      app.UseMvcWithDefaultRoute();
    }
  }
}
